package com.nagarro.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class TestProgram1 
{
	WebDriver driver;
	SoftAssert softAssert = new SoftAssert(); 
	
	@BeforeTest
	public void beforeTest() 
	{	
        System.setProperty("webdriver.chrome.driver", "E:\\Jenkins_Demo\\chromedriver_win32\\chromedriver.exe");
    	driver = new ChromeDriver();
		System.out.println("Launching browser");
    }	
  
	@Test(priority=1)
 	public void testEasy() 
    {	
		driver.get("http://demo.guru99.com/test/guru99home/");  
		String Actualtitle = driver.getTitle();
		String Expectedtitle="Demo Guru99 Page";
		System.out.println("Before Assetion: Expected Result: " + Expectedtitle +", Actual result: "+ Actualtitle);
		softAssert.assertEquals(Actualtitle, Expectedtitle);
		System.out.println("After Assertion: Expected Result: " + Expectedtitle +", Actual result: "+ Actualtitle + " Title matched ");
	}	
	
	@Test(priority=2)
	public void clickNewtour() throws InterruptedException
	{
		driver.get("http://demo.guru99.com/test/newtours/");  
		System.out.println("Clicking on New Tours button");
		Thread.sleep(500);
		WebElement newTourButton = driver.findElement(By.xpath("//table/tbody/tr/td/a[text()='SIGN-ON']"));
		softAssert.assertTrue(newTourButton.isDisplayed());
		System.out.println("Sign-On button is visible");
		Thread.sleep(200);
	}
		
	@AfterTest
	public void afterTest() 
	{
		softAssert.assertAll();
		driver.quit();			
	}	
}
